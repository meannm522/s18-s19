//[SECTION] Objects

  //Objects -> is a collection of related data and/or functionalities. the purpose of an object is not only to store multiple  data-sets but also to represent real world objects.

  //SYNTAX: 
      // let/const variableName = {
      // 	key/property: value
      // }

      //Note: Information stored in objects are represented in key:value pairing. 

   //lets use this example to describe a real-world item/object
   let cellphone = {
   	 name: 'Nokia 3210',
   	 manufactureDate: '1999',
   	 price: 1000 
   }; //'key' -> is also mostly referred to as a 'property' of an object. 

   console.log(cellphone); 
   console.log(typeof cellphone); 

   //How to store multiple objects, 

   //you can use an array struture to store them.
   let users = [
	   { 
	   	 name: 'Anna', 
	   	 age: '23'
	   },
	   { 
	   	 name: 'Nicole', 
	   	 age: '18'
	   },
	   { 
	   	 name: 'Smith', 
	   	 age: '42'
	   },
	   { 
	   	 name: 'Pam', 
	   	 age: '13'
	   },
	   { 
	   	 name: 'Anderson', 
	   	 age: '26'
	   }
  	];

   console.log(users); 
   //now there is an alternative way when displaying values of an object collection
   console.table(users); 

   //Complex examples of JS objects

   //NOTE: Different data types may be stored in an object's property creating more complex data structures

   //When using JS Objects, you can also insert 'Methods'.

   //Methods -> are useful for creating reusable functions that can perform tasks related to an object. 
   let friend = {
   	 //properties
   	 firstName: 'Joe', //String or numbers
   	 lastName: 'Smith', 
   	 isSingle: true, //boolean
     emails: ['joesmith@gmail.com', 'jsmith@company.com'], //array
     address: { //Objects
     	city: 'Austin',
     	state: 'Texas',
     	country: 'USA'
     },
     //methods 
     introduce: function() {
     	console.log('Hello Meet my new Friend'); 
     },
     advice: function() {
     	console.log('Give friend an advice to move on');
     }, 
     wingman: function() {
     	console.log('Hype friend when meeting new people');
     }
   }
   console.log(friend);

   // use the 'this' keyword to associate each property with the global object and bind them with the respective value
   	function Pokemon(name, type, level, trainer) {
		//properties
		this.pokemonName = name; 
		this.pokemonType = type; 
		this.pokemonHealth = 2 * level;  
		this.pokemonLevel = level; 
		this.owner = trainer; 
		//methods
		this.tackle = function(target){
			console.log(this.pokemonName + ' tackled ' + target.pokemonName); 
		};
		this.greetings = function(){
			console.log(this.pokemonName + ' says hello!');
		};
	}

	//create instance of a pokemon using the constructor function.
	let pikachu = new Pokemon('Pikachu', 'electric', 3, 'Ash ketchup');
	let ratata = new Pokemon('Ratata', 'normal', 4, 'Misty');
    let snorlax = new Pokemon('Snorlax', 'normal', 5, 'Gary Oak'); 

    //display all pokemons in the console in a table format
    let pokemons = [pikachu, ratata, snorlax]; 

    console.table(pokemons); 
// section how to access object properties
// using '.' dot notation you can access properties of an object

// syntax: objectName.propertyName
console.log(snorlax.owner);
console.log(pikachu.pokemonType);

// practice accessing a method of an object.
pikachu.tackle(ratata)
snorlax.greetings();

// [Alternate approach]
// [] square bracketts as an alternate approach.
// SYNTAX: objectName['property name']
console.log(ratata['owner']);
console.log(ratata[['pokemonLevel']]);



// which is the BEST USE CASE dot notatoin OR Square bracket?
// we will stick with dot notation when accessing properties of an object.  as our convention when managing porperites of an object

// we will use square brackets when dealing with indexes of an array.

// additional knowledge
// keep in mind, you can write and declare objects this way
// to create an objects, you will use object literals'{}'

let trainer = {}; //empty object
console.log(trainer);

// will i be able to add properties from outside an object? YES
trainer.name ='Ash Ketchup';
console.log(trainer);
trainer.friends = ['Misty', 'Brock', 'Tracey'];
console.log(trainer); //array
//method
trainer.speak = function(){
	console.log('Pikachu, I choose you!');
}
trainer.speak();